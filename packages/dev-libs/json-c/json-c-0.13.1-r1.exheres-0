# Copyright 2011 Paul Seidler <sepek@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require alternatives

SUMMARY="Reference counting object model that lets you construct JSON objects in C"
HOMEPAGE="https://github.com/${PN}/${PN}/wiki"
DOWNLOADS="https://s3.amazonaws.com/${PN}_releases/releases/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-libs/json-c:0[<0.13.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-rdrand
    --enable-threading
    --disable-static
)

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    edo sed \
        -e "s:-Werror::" \
        -i configure.ac

    autotools_src_prepare
}

src_install() {
    local local arch_dependent_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.la        lib${PN}-${SLOT}.la
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

pkg_preinst() {
    if has_version dev-libs/json-c ; then
        # Older versions installed to /usr/include/json, 0.11 installs a symlink
        [[ -d "${ROOT}"/usr/include/json ]] && edo rm -r "${ROOT}"/usr/include/json
    fi
}

